﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskCaller.Models;

namespace TaskCaller.Services
{
    public static class TaskCallerServiceCollectionExtensions
    {
        public static IServiceCollection AddTaskCaller(this IServiceCollection services, IConfiguration configuration)
        {
            
            

            //配置
            services.Configure<ConnectionStringsConfig>(configuration.GetSection("ConnectionStrings"));
            services.Configure<CallerServerConfig>(configuration.GetSection("CallerServer"));

            //httpfactory
            services.AddHttpClient();

            //业务逻辑服务
            services.AddSingleton<DelayTaskService>();
            services.AddSingleton<ExecuteLogService>();
            services.AddSingleton<TimedTaskService>();

            //任务管理服务
            services.AddSingleton<DelayTaskManager>();
            services.AddSingleton<TimedTaskManager>();

            //托管服务
            services.AddHostedService<TaskCallerHostedService>();

            return services;
        }
    }
}
