﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using TaskCaller.Commons;
using TaskCaller.Models;
using TaskCaller.Models.entity;

namespace TaskCaller.Services
{
    public class DelayTaskManager
    {
        int _loadIntervalCount = 0;
        int _loadIntervalSeconds = 0;
        DelayTaskService _delayTaskService;
        ExecuteLogService _executeLogService;
        IHttpClientFactory _httpClientFactory;
        TimeWheel _tw;
        ILogger _logger;
        public DelayTaskManager(DelayTaskService delayTaskService, ExecuteLogService executeLogService, IHttpClientFactory httpClientFactory, IOptionsMonitor<CallerServerConfig> config, ILogger<DelayTaskManager> logger)
        {
            _delayTaskService = delayTaskService;
            _executeLogService = executeLogService;
            _httpClientFactory = httpClientFactory;
            _logger = logger;

            _tw = new TimeWheel(config.CurrentValue.TimeWheelSlotNum);
            _loadIntervalCount = _loadIntervalSeconds = config.CurrentValue.LoadDelayTaskIntervalSeconds;
        }

        public void LoadTask(DateTime dateTime)
        {
            if (_loadIntervalSeconds > 0)
            {
                if (_loadIntervalCount >= _loadIntervalSeconds)
                {
                    _loadIntervalCount = 0;
                }
                if (_loadIntervalCount == 0)
                {
                    //加载任务
                    var list = _delayTaskService.SelectReadyList(dateTime, dateTime.AddMinutes(1));
                    _logger.LogDebug("加载DelayTask条数:" + list.Count);
                    foreach (var item in list)
                    {
                        _tw.Add(item.Id, item.TriggerTime);
                    }
                }
                _loadIntervalCount++;
            }
        }

        public void TriggerTask(DateTime dateTime)
        {
            //触发
            var delayTaskIds = _tw.WalkStep();
            _logger.LogDebug("触发DelayTask条数:" + delayTaskIds.Count);
            if (delayTaskIds.Count > 0)
            {
                var taskList = _delayTaskService.SelectByIds(delayTaskIds);
                if (taskList.Count > 0)
                {
                    taskList.AsParallel().ForAll(Run);
                }
            }
        }

        private void Run(DelayTask task)
        {
            var hc = _httpClientFactory.CreateClient();
            hc.Timeout = task.TimeoutSeconds == 0 ? TimeSpan.FromSeconds(15) : TimeSpan.FromSeconds(task.TimeoutSeconds);

            Task<HttpResponseMessage> requestTask;
            if (task.Method.Equals("GET", StringComparison.OrdinalIgnoreCase))
            {
                requestTask = hc.GetAsync(task.Url);
            }
            else
            {
                StringContent stringContent = new StringContent(task.PostData, Encoding.UTF8);
                requestTask = hc.PostAsync(task.Url, stringContent);
            }

            requestTask.ContinueWith(reqTask =>
            {
                var log = new ExecuteLog()
                {
                    PostData = task.PostData,
                    TaskId = task.Id,
                    TaskName = task.Name,
                    TaskUrl = task.Url,
                    TaskType = 2,
                    TaskMethod = task.Method,
                };

                if (reqTask.IsFaulted)
                {
                    log.Status = 2;
                    log.Message = reqTask.Exception.GetBaseException().StackTrace;
                    _executeLogService.Add(log);
                    _logger.LogError(reqTask.Exception.InnerExceptions.FirstOrDefault(), "执行DelayTask异常，id:{0},name:{1},url:{2}", task.Id, task.Name, task.Url);
                    ReJoin(task);
                }
                else if (reqTask.IsCanceled)
                {
                    log.Status = 3;
                    log.Message = "timeout";
                    _executeLogService.Add(log);
                    ReJoin(task);
                }
                else
                {
                    reqTask.Result.Content.ReadAsStringAsync().ContinueWith(readTask =>
                    {
                        if (readTask.Result == task.SuccessFlag)
                        {
                            //删除数据
                            _delayTaskService.Delete(task.Id);
                            log.Status = 1;
                            log.Message = "success";
                            _executeLogService.Add(log);
                        }
                        else
                        {
                            log.Status = 2;
                            log.Message = readTask.Result;

                            _logger.LogError("执行DelayTask出错，id:{0},name:{1},url:{2},msg:{3}", task.Id, task.Name, task.Url, readTask.Result);
                            _executeLogService.Add(log);
                            ReJoin(task);
                        }
                    });
                }
            });
        }

        private void ReJoin(DelayTask task)
        {
            var flag = _delayTaskService.Retry(task);
            if (flag)
            {
                _tw.Add(task.Id, task.RetrySeconds);
            }
            else
            {
                //如果不重试，是否要删除
                _delayTaskService.Delete(task.Id);
            }
        }
    }
}
