﻿using Microsoft.AspNetCore.Mvc;
using TaskCaller.Services;
using TaskCaller.UI;

namespace SchedulerLite.Manager.Controllers
{
    public class ExecuteLogController : Controller
    {

        ExecuteLogService _executeLogService;
        public ExecuteLogController(ExecuteLogService executeLogService)
        {
            _executeLogService = executeLogService;
        }

        // GET: ExecuteLog
        public ActionResult List(long taskId = 0, int taskType = 0, int status = 0, int page = 1)
        {
            var pageSize = 10;
            var plist = _executeLogService.SearchList(taskId, taskType, status, page, pageSize);
            ViewBag.plist = plist.ToStaticPagedList();
            return View();
        }

    }
}