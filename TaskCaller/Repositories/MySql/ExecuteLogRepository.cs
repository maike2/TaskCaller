﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loogn.OrmLite;
using Microsoft.Extensions.Options;
using TaskCaller.Models;
using TaskCaller.Models.entity;

namespace TaskCaller.Repositories.MySql
{
    public class ExecuteLogRepository : BaseRepository, IExecuteLogRepository
    {
        public ExecuteLogRepository(IOptions<ConnectionStringsConfig> options) : base(options)
        {
        }

        public long Add(ExecuteLog m)
        {
            using (var db = Open())
            {
                return db.Insert(m);
            }
        }

        public OrmLitePageResult<ExecuteLog> SearchList(long taskId, int taskType, int status, int pageIndex, int pageSize)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("1=1");
            if (taskId > 0)
            {
                sb.AppendFormat(" and TaskId={0}", taskId);
            }
            if (taskType > 0)
            {
                sb.AppendFormat(" and TaskType={0}", taskType);
            }
            if (status > 0)
            {
                sb.AppendFormat(" and Status={0}", status);
            }
            using (var db = Open())
            {
                return db.SelectPage<ExecuteLog>(new OrmLitePageFactor
                {
                    Conditions = sb.ToString(),
                    OrderBy = "id desc",
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                });
            }
        }
    }
}
