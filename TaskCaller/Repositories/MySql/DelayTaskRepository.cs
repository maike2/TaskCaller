﻿using Loogn.OrmLite;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using TaskCaller.Models;
using TaskCaller.Models.entity;

namespace TaskCaller.Repositories.MySql
{
    public class DelayTaskRepository : BaseRepository, IDelayTaskRepository
    {
        public DelayTaskRepository(IOptions<ConnectionStringsConfig> options) : base(options)
        {
        }

        public long Add(DelayTask m)
        {
            using (var db = Open())
            {
                return db.Insert(m);
            }
        }

        public long Update(DelayTask m)
        {
            using (var db = Open())
            {
                return db.Update(m, "Name", "Url", "Method", "PostData", "Enable",
                    "TriggerTime", "MaxRetryCount", "RetrySeconds", "SuccessFlag", "TimeoutSeconds");
            }
        }

        public OrmLitePageResult<DelayTask> SearchList(string name, int pageIndex, int pageSize)
        {
            var condition = "1=1";
            name = SqlInjection.Filter(name);
            if (!string.IsNullOrEmpty(name))
            {
                condition += " and Name like '" + name + "'";
            }
            using (var db = Open())
            {
                return db.SelectPage<DelayTask>(new OrmLitePageFactor
                {
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    OrderBy = "id desc",
                    Conditions = condition,
                });
            }
        }

        public DelayTask SingleById(long id)
        {
            using (var db = Open())
            {
                return db.SingleById<DelayTask>(id);
            }
        }

        public int Delete(long id)
        {
            using (var db = Open())
            {
                return db.DeleteById<DelayTask>(id);
            }
        }

        public List<DelayTask> SelectReadyList(DateTime beginTime, DateTime endTime)
        {
            using (var db = Open())
            {
                var sql = string.Format("select Id, TriggerTime from DelayTask WHERE Enable=1 and TriggerTime >= '{0}' AND TriggerTime<'{1}'", beginTime.ToString("yyyy-MM-dd HH:mm:ss"), endTime.ToString("yyyy-MM-dd HH:mm:ss"));
                //Console.WriteLine(sql);
                return db.Select<DelayTask>(sql);
            }
        }

        public List<DelayTask> SelectByIds(List<long> ids)
        {
            using (var db = Open())
            {
                return db.SelectByIds<DelayTask>(ids);
            }
        }

        public int RetryUpdate(DelayTask task)
        {
            using (var db = Open())
            {
                var flag = db.Update<DelayTask>(DictBuilder
                    .Assign("TriggerTime", task.TriggerTime)
                    .Assign("$ExecCount", "ExecCount+1"), "Id=" + task.Id, null);
                return flag;
            }
        }

    }
}
